jQuery(document).ready(function($) {
     // $(".logo").click(function() {
       //  var windowWidth = $(window).width();
         // if (windowWidth >= 640) {
            // 获取当前url中的路径名，相当于/yx2dev/web/...
            //var pathName = window.location.pathname;

            //获取带"/"的web项目名，我们的项目基于durpal的composer安装，项目名中含有2个"/",如/yx2dev/web/，将来可能要调整。
            // var projNameArr = pathName.split("/");
            // if (projNameArr.length >= 3) { // 防止数组越界。
            //     var projectName = "/" + projNameArr[1] + "/" + projNameArr[2];
            // }

            // 如果是带一个"/"的项目名，如：/docroot/,可用下面的方法提取项目名。
            // var projectName = pathName.substring(0, pathName.substr(1).indexOf('/') + 1);

            //web项目根目录, window.location.origin相当于http://wx.shxyyx.com，projectName相当于/yx2dev/web/
            //window.location.href = window.location.origin + projectName;

            // 2020-06-03 14:29:05
            // 果不其然，以上方法还是不能通用，以下方法也不能通用，以后还是要改动。
            //  var webPos = location.href.indexOf("web"); // 以后也可能是docroot等等，所有不能通用。而且可能有多个web目录。
             // window.location.href = window.location.href.substring(0, webPos + 3);

        //  }
     // })
    if ($("#edit-billing-information-profile-edit-button")) {
        $("#edit-billing-information-profile-edit-button").trigger("mousedown");
    };
    if ($("#edit-completed-min")) {
        $("#edit-completed-min").attr("type", "date")
    }
    if ($("#edit-completed-max")) {
        $("#edit-completed-max").attr("type", "date")
    }
    if ($("#edit-billing-information-profile .field--name-field-authorization-card .fieldset-wrapper div")) {
        setTimeout(
            function() {
                var author_card = $("#edit-billing-information-profile .field--name-field-authorization-card .fieldset-wrapper div").html();
                if (author_card == "") {
                    $("#edit-billing-information-profile .field--name-field-authorization-card .fieldset-wrapper div").html("您还没有授权卡，请先添加授权卡。");
                    $("#commerce-checkout-flow-multistep-default #edit-actions #edit-actions-next").attr("disabled", true);
                }
                $("#edit-billing-information-profile .js-form-item-billing-information-profile-copy-to-address-book input").removeAttr("checked");
                // if($("#edit-billing-information-profile .js-form-item-billing-information-profile-copy-to-address-book")){
                //   
                //   $("#edit-billing-information-profile .js-form-item-billing-information-profile-copy-to-address-book").css("display","none");
                // }
            }, 2000);
    }
    $("input[placeholder='起始日期']") && $("input[placeholder='起始日期']").attr("type","date");
    $("input[placeholder='结束日期']") && $("input[placeholder='结束日期']").attr("type","date");
    $("input[placeholder='日期']") && $("input[placeholder='日期']").attr("type","date");
    
    
    //菜单栏--鼠标移动到2，3级菜单，1级菜单仍保持高亮
    // $("#mainmenu>.dropdown").addClass("first_step");
    $("#mainmenu>.dropdown").hover(function() {
        $(this).children("a").css({ "color": "#e54e4b", "background": "#eee" });
    })
    $("#mainmenu>.dropdown").mouseout(function() {
        if ($(this).children("a").hasClass("is-active")) {
            $(this).children("a").css({ "color": "#fff", "background": "#e54e4b" });
        } else if ($(this).find(".dropdown-menu").css("display") == "block") {
            $(this).children("a").css({ "color": "#e54e4b", "background": "#eee" });
        } else {
            $(this).children("a").css({ "color": "#ffffff", "background": "transparent" });
        }
    })
    $("#mainmenu>.dropdown>.dropdown-menu").mouseout(function() {
        $(this).parent().children("a").css({ "color": "#ffffff", "background": "transparent" });
    })
    $("#mainmenu>.dropdown>.dropdown-menu>.dropdown>.dropdown-menu").mouseout(function() {
            $(this).parent().parent().children("a").css({ "color": "#ffffff", "background": "transparent" });
        })
        //菜单栏阻止事件冒泡触发返回首页
    function stopPropagation(e) {
        e = e || window.event;
        if (e.stopPropagation) { //W3C阻止冒泡方法 
            e.stopPropagation();
        } else {
            e.cancelBubble = true; //IE阻止冒泡方法 
        }
    }
    $("#mainmenu>.dropdown>.dropdown-menu>.dropdown>.dropdown-menu li a").click(function(event) {
        stopPropagation(event);
    })
    // // 点击子菜单，一级菜单高亮
    // $("#mainmenu .dropdown .dropdown a").click(function(){
    //   $(this).parents('.first_step').children("a").addClass("is-active");
    // })

    gohome=function (projectName){
        window.location.href = window.location.origin + projectName.toString();
     }

});